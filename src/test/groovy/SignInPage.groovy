import geb.Page
import org.openqa.selenium.By

class SignInPage extends Page {
    static at = { title == "Sign in to GitHub · GitHub" }

    static content = {
        form { $(By.xpath("/html/body/div[3]/div[1]/div/form")) }
    }
}
