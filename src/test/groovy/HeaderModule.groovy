import geb.Module
import org.openqa.selenium.By

class HeaderModule extends Module {
    static content = {
        signin { $(By.xpath("/html/body/div[1]/header/div/div[2]/div/span/div/a[1]")) }
    }
}
