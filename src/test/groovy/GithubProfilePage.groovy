import geb.Page

class GithubProfilePage extends Page {
    static url = "https://github.com/tkob"

    static content = {
        header { module(HeaderModule) }
    }
}
