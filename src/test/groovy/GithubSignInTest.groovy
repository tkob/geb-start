import geb.junit4.GebReportingTest
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4)
class GithubSignInTest extends GebReportingTest {

    @Test
    void testGithub() {
        to GithubProfilePage
        writeGebReport()

        header.signin.click()

        at SignInPage
        writeGebReport()

        form.login = "tkob"
        form.password = ""
        writeGebReport()

        form.commit().click()
    }
}
