import org.openqa.selenium.ie.InternetExplorerDriver

baseUrl = "http://gebish.org"

// default driver...
def ieDriver = new File('src/test/resources/IEDriverServer.exe')
System.setProperty('webdriver.ie.driver', ieDriver.absolutePath)
driver = { new InternetExplorerDriver() }

environments {
    // specify environment via -Dgeb.env=ie
    'ie' {
        /*def*/ ieDriver = new File('src/test/resources/IEDriverServer.exe')
        System.setProperty('webdriver.ie.driver', ieDriver.absolutePath)
        driver = { new InternetExplorerDriver() }
    }
}

reportsDir = "build/geb-reports"

waiting {
    timeout = 2
}
